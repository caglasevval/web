﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace weeeb.Models
{
    public class Yorum
    {
        public int Id { get; set; }

        public string KullaniciAdi { get; set; }

        public string KullaniciId { get; set; }

        public string MakaleAdi { get; set; }

        public DateTime Tarih { get; set; }

        public virtual Kullanici Kullanicilar { get; set; }
    }
}