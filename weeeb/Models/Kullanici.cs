﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace weeeb.Models
{
    public class Kullanici
    {
        public int Id { get; set; }

        public string Adi { get; set; }

        public string Soyadi { get; set; }

        public int Yas { get; set; }

        public int Boy { get; set; }

        public string Email { get; set; }

        public string Sifre { get; set; }

        public virtual ICollection<Kullanici> Kullanicilar { get; set; }
    }
}