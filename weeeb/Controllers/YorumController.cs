﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using weeeb.Models;
using weeeb.Veri;

namespace weeeb.Controllers
{
    public class YorumController : Controller
    {
        // GET: Yorum

        IdariContext islemler = new IdariContext();
        public ActionResult Index()
        {
            List<Yorum> yorumlar = islemler.Yorumlar.ToList();
            return View(yorumlar);
        }
    }
}