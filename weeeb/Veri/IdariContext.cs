﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using weeeb.Models;

namespace weeeb.Veri
{
    public class IdariContext : DbContext
    {
        public IdariContext() : base("IdariVeritabani")
        {

        }

        public DbSet<Kullanici> Kullanicilar { get; set; }

        public DbSet<Yorum> Yorumlar { get; set; }

    }
}